import request from 'supertest';

import app from '../src/app';

describe('GET /api/product/', () => {
  it('responds with a json message and array with data', (done) => {
    request(app)
      .get('/api/product/')
      .set('Accept', 'application/json')
      .expect(200, {
        message: 'retrieved all products',
        data: [],
        statusCode: 200,
      }, done);
  });
});

describe('POST /api/product/add', () => {
  it('responds with a json message', (done) => {
    request(app)
      .post('/api/product/add')
      .send({
        'name': 'Dress',
        'price': 20.65,
      })
      .set('Accept', 'application/json')
      .expect(200, {
        message: 'new product added',
        statusCode: 200,
      }, done);
  });
});


describe('POST /api/product/remove/:id', () => {
  it('responds with a json message', (done) => {
    request(app)
      .delete('/api/product/remove/1')
      .set('Accept', 'application/json')
      .expect(200, {
        message: 'removed product',
        statusCode: 200,
      }, done);
  });
});