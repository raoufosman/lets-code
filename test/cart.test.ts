import request from 'supertest';

import app from '../src/app';
import { ICart } from '../src/models/cart.model';
import { calculateCart } from '../src/services/cart.service';

describe('GET /api/cart/', () => {
  it('responds with a json message and array with data', (done) => {
    request(app)
      .get('/api/cart/')
      .set('Accept', 'application/json')
      .expect(200, {
        message: 'retrieved all products in cart',
        statusCode: 200,
        data: { items: [], totalQuantity: 0, totalPrice: 0 },
      }, done);
  });
});

describe('POST /api/cart/add', () => {
  it('responds with a json message', (done) => {
    request(app)
      .post('/api/cart/add')
      .send({
        'productId': 1,
        'quantity': 2,
      })
      .set('Accept', 'application/json')
      .expect(200, {
        message: 'added item to cart',
        statusCode: 200,
      }, done);
  });
});


describe('POST /api/cart/remove/:id', () => {
  it('responds with a json message', (done) => {
    request(app)
      .delete('/api/cart/remove/1')
      .set('Accept', 'application/json')
      .expect(200, {
        message: 'removed item to cart',
        statusCode: 200,
      }, done);
  });
});

describe('3 T-shirts should equal 25.98', () => {
  const cart: ICart = {
    items: [{
      'shoppingCartItemId': 1,
      'productId': 1,
      'name': 'T-Shirt',
      'price': 12.99,
      'quantity': 3,
    }],
  };

  const { totalPrice, totalQuantity } = calculateCart(cart);
  expect(totalPrice).toBe(25.98);
  expect(totalQuantity).toBe(2);
});

describe('2 T-shirts and 2 jeans should equal 62.99', () => {
  const cart: ICart = {
    items: [{
      'shoppingCartItemId': 1,
      'productId': 1,
      'name': 'T-Shirt',
      'price': 12.99,
      'quantity': 2,
    },
    {
      'shoppingCartItemId': 1,
      'productId': 2,
      'name': 'Jeans',
      'price': 25.00,
      'quantity': 2,
    }],
  };

  const { totalPrice, totalQuantity } = calculateCart(cart);
  expect(totalPrice).toBe(62.99);
  expect(totalQuantity).toBe(3);
});

describe('1 T-shirt, 2 Jeans and 3 Dress should equal 91.30', () => {
  const cart: ICart = {
    items: [{
      'shoppingCartItemId': 1,
      'productId': 1,
      'name': 'T-Shirt',
      'price': 12.99,
      'quantity': 1,
    },
    {
      'shoppingCartItemId': 2,
      'productId': 2,
      'name': 'Jeans',
      'price': 25.00,
      'quantity': 2,
    },
    {
      'shoppingCartItemId': 3,
      'productId': 3,
      'name': 'Dress',
      'price': 20.65,
      'quantity': 3,
    }],
  };

  const { totalPrice, totalQuantity } = calculateCart(cart);
  expect(totalPrice).toBe(91.30);
  expect(totalQuantity).toBe(4);
});