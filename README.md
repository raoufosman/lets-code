## Let's code 

This project uses a sqlite in-memory database. 

### Running the project
Make sure to add `.env` file at the root of the project that includes `NODE_ENV` env variable.

##### Installing dependencies
```bash
npm install
```

##### Running API
API runs on port 5000. If want to change the port, you can add the `PORT` env variable.

```bash
npm run dev
```

##### Running Tests
```bash
npm run test
```

##### Linting
```bash
npm run lint
```

### Design

#### Tech Stack
This project utilizes typescript to leverage type saftey and express framework to provide robust HTTP features. For data persistance, this project uses sqlite in-memory database to store products and persist shopping cart details. 

#### Project Design
This project utilizes a repository pattern for data layer and service oriented architecture to manage complex logic. The directory structure shows that we separated key software components at the API, Database, Models, Services, and Tests level. 

API - This directory has the routes that are exposed via our Rest API.
Database - This directory has the repositories and data access components that are used to communicate with the data layer.
Models - This directory has the objects that are represented at the data layer and API response schemas.
Services - this directory has the classes that hold complex logic. 
Tests - this directory has the unit tests for the project.

This project utilizes eslint to make sure software conforms to best coding standards.

#### Architecture
This project utilizes sqlite3 in-memory database to provide data persistance. Leveraging sqlite3 allows a lightweight data layer that allows us to achieve the requirements considering performance and small footprint. 

We chose expressjs to due to its popularity, ease of use, and great documentation. This project was bootstrapped using [express-api-starter](https://github.com/w3cj/express-api-starter.git) to get a feature rich starter project to build on.

One thing we had to add was Joi - Json schema validation. Adding this at the request level allows us to validate request body, query parameters, and url parameters to make sure request passed is validated in middleware.

We added middlewares to make sure any url that is not supported does return a 404 and all errors are handled gracefully.