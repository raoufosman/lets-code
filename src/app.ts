import express, { Request, Response } from 'express';
import morgan from 'morgan';
import helmet from 'helmet';
import cors from 'cors';

import * as middlewares from './middlewares';
import cartApi from './api/cart';
import productApi from './api/product';
import { init } from './database/sqlite';

require('dotenv').config();

const app = express();

app.use(morgan('dev'));
app.use(helmet());
app.use(cors());
app.use(express.json());

app.get('/', (req: Request, res: Response) => {
  res.json({
    message: 'Welcome to Shopping cart API',
    statusCode: res.statusCode,
  });
});

app.use('/api/cart', cartApi);
app.use('/api/product', productApi);

app.use(middlewares.notFound);
app.use(middlewares.errorHandler);

init();

export default app;
