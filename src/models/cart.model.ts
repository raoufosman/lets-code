export interface ICart {
  items?: ICartItem[],
  discountApplied?: boolean;
}

export interface ICartItem {
  shoppingCartItemId?: number,
  productId: number,
  name?: string;
  price?: number;
  quantity: number;
  discountQuantity?: number;
  total?: number;
}