import express, { Request, Response } from 'express';
import { ShoppingCartRepository } from '../database/shoppingCart.repository';
import { ICartItem } from '../models/cart.model';
import { calculateCart } from '../services/cart.service';
import * as Joi from 'joi';

import {
  ContainerTypes,
  // Use this as a replacement for express.Request
  ValidatedRequest,
  // Extend from this to define a valid schema type/interface
  ValidatedRequestSchema,
  // Creates a validator that generates middlewares
  createValidator,
} from 'express-joi-validation';

const validator = createValidator();
const addItemSchema = Joi.object({
  productId: Joi.number().required(),
  quantity: Joi.number().required(),
});

interface AddItemSchema extends ValidatedRequestSchema {
  [ContainerTypes.Body]: {
    shoppingCartItemId?: number,
    productId: number,
    quantity: number;
  }
}

const removeItemSchema = Joi.object({
  id: Joi.number().required(),
});

interface RemoveItemSchema extends ValidatedRequestSchema {
  [ContainerTypes.Query]: {
    id: number,
  }
}

const router = express.Router();
const repository = new ShoppingCartRepository();

router.get('/', async (req: Request, res: Response) => {
  try {
    const cart = await repository.getCart();
    const { totalPrice, totalQuantity } = calculateCart(cart);

    res.status(200);
    return res.json({
      message: 'retrieved all products in cart',
      statusCode: res.statusCode,
      data: {
        ...cart,
        totalQuantity,
        totalPrice,
      },
    });
  } catch (e: any) {
    res.status(500);
    return res.json({
      message: e.message,
      stackTrace: process.env.NODE_ENV === 'production' ? '' : e.stack,
    });
  }
});

router.post(
  '/add',
  validator.body(addItemSchema),
  async (req: ValidatedRequest<AddItemSchema>, res: Response) => {
    try {
      const item = req.body;

      if (item == null) {
        res.status(400);
        return res.json({
          statusCode: res.statusCode,
          message: 'item cannot be null',
        });
      }

      if (item.shoppingCartItemId && item.shoppingCartItemId > 0) {
        res.status(400);
        return res.json({
          statusCode: res.statusCode,
          message: 'this item is already in cart',
        });
      }

      if (!item.productId || !item.quantity) {
        res.status(400);
        return res.json({
          statusCode: res.statusCode,
          message: 'required fields are missing to add an item to shopping cart',
        });
      }

      var shoppingCartItem: ICartItem = {
        productId: item.productId,
        quantity: item.quantity,
      };

      await repository.addItemToCart(shoppingCartItem);
      res.status(200);
      return res.json({
        statusCode: res.statusCode,
        message: 'added item to cart',
      });
    } catch (e: any) {
      res.status(500);
      return res.json({
        message: e.message,
        stackTrace: process.env.NODE_ENV === 'production' ? '' : e.stack,
      });
    }
  });

router.delete(
  '/remove/:id',
  validator.params(removeItemSchema),
  async (req: ValidatedRequest<RemoveItemSchema>, res: Response) => {
    try {
      var id = Number(req.params.id);

      if (id == 0 || id < 0) {
        res.status(400);
        return res.json({
          statusCode: res.statusCode,
          message: 'id must be greater than 0 to remove from cart',
        });
      }

      await repository.removeItemFromCart(id);
      res.status(200);
      return res.json({
        statusCode: res.statusCode,
        message: 'removed item to cart',
      });
    } catch (e: any) {
      res.status(500);
      return res.json({
        message: e.message,
        stackTrace: process.env.NODE_ENV === 'production' ? '' : e.stack,
      });
    }
  });

export default router;
