import express, { Request, Response } from 'express';
import { ProductRepository } from '../database/product.repository';
import { IProduct } from '../models/product.model';
import * as Joi from 'joi';

import {
  ContainerTypes,
  // Use this as a replacement for express.Request
  ValidatedRequest,
  // Extend from this to define a valid schema type/interface
  ValidatedRequestSchema,
  // Creates a validator that generates middlewares
  createValidator,
} from 'express-joi-validation';

const validator = createValidator();
const addProductSchema = Joi.object({
  name: Joi.string().required(),
  price: Joi.number().required(),
});

interface AddProductSchema extends ValidatedRequestSchema {
  [ContainerTypes.Body]: {
    name: string,
    price: number;
  }
}

const removeProductSchema = Joi.object({
  id: Joi.number().required(),
});

interface RemoveProductSchema extends ValidatedRequestSchema {
  [ContainerTypes.Query]: {
    id: number,
  }
}

const router = express.Router();
const repository = new ProductRepository();

router.get('/', async (req: Request, res: Response) => {
  try {
    const results = await repository.getProducts();
    res.status(200);
    return res.json({
      message: 'retrieved all products',
      statusCode: res.statusCode,
      data: results,
    });
  } catch (e: any) {
    res.status(500);
    return res.json({
      message: e.message,
      stackTrace: process.env.NODE_ENV === 'production' ? '' : e.stack,
    });
  }
});

router.post(
  '/add',
  validator.body(addProductSchema),
  async (req: ValidatedRequest<AddProductSchema>, res: Response) => {
    try {
      const item = req.body;

      if (item == null) {
        res.status(400);
        return res.json({
          statusCode: res.statusCode,
          message: 'item cannot be null',
        });
      }

      var product: IProduct = {
        name: item.name,
        price: item.price,
      };

      await repository.addProduct(product);
      res.status(200);
      return res.json({
        statusCode: res.statusCode,
        message: 'new product added',
      });
    } catch (e: any) {
      res.status(500);
      return res.json({
        message: e.message,
        stackTrace: process.env.NODE_ENV === 'production' ? '' : e.stack,
      });
    }
  });

router.delete(
  '/remove/:id',
  validator.params(removeProductSchema),
  async (req: ValidatedRequest<RemoveProductSchema>, res: Response) => {
    try {
      var id = Number(req.params.id);

      if (id == 0 || id < 0) {
        res.status(400);
        return res.json({
          statusCode: res.statusCode,
          message: 'id must be greater than 0 to product',
        });
      }

      var exists = await repository.isProductInShoppingCart(id);

      if (exists) {
        res.status(400);
        return res.json({
          statusCode: res.statusCode,
          message: "product is in your cart. Cannot remove until it's removed from cart",
        });
      }

      await repository.removeProduct(id);
      res.status(200);
      return res.json({
        statusCode: res.statusCode,
        message: 'removed product',
      });
    } catch (e: any) {
      res.status(500);
      return res.json({
        message: e.message,
        stackTrace: process.env.NODE_ENV === 'production' ? '' : e.stack,
      });
    }
  });

export default router;