import sqlite3 from 'sqlite3';
const db = new sqlite3.Database(':memory:');

export const init = () => {
  try {
    db.run(`create table if not exists products (
      id integer primary key autoincrement,
      name text not null,
      price decimal(3, 2) not null
      )`);

    db.run(`create table if not exists shopping_cart (
      id integer primary key autoincrement,
      product_id text not null,
      quantity tinyint not null,
      foreign key (product_id) references products (id) 
      )`);


    console.log('initialized database with shopping cart table and products table');
  } catch (e) {
    console.error('error initializing in memory sqlite database');
    throw e;
  }
};

export const findAll = async (query: string): Promise<any[]> => {
  return new Promise((resolve, reject) => {
    db.all(query, (error, results: any[]) => {
      if (error) {
        console.error('error selecting all from table');
        console.error(error);
        reject(error);
      }

      return resolve(results);
    });
  });
};

export const findOne = async (query: string, id: string): Promise<any[]> => {
  return new Promise((resolve, reject) => {
    db.get(query, id, (error, result: any) => {
      if (error) {
        console.error('error selecting all from table');
        console.error(error);
        reject(error);
      }

      return resolve(result);
    });
  });
};

export const save = async (query: string, params: string[]): Promise<void> => {
  return new Promise((resolve, reject) => {
    db.run(query, params, (error) => {
      if (error) {
        console.error('error inserting into table');
        reject(error);
      }

      resolve();
    });
  });
};

export const remove = async (query: string, id: number): Promise<void> => {
  return new Promise((resolve, reject) => {
    db.run(query, id, (error) => {
      if (error) {
        console.error('error inserting into table');
        reject(error);
      }

      resolve();
    });
  });
};