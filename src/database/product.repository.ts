import { IProduct } from '../models/product.model';
import { findAll, save, remove, findOne } from './sqlite';

export class ProductRepository {
  async getProducts(): Promise<IProduct[]> {
    try {
      const results = await findAll('select id, name, price from products');
      console.log(results);
      if (results && results.length > 0) {
        return results as IProduct[];
      }

      return [] as IProduct[];
    } catch (e) {
      console.error('error finding all items in cart');
      throw e;
    }
  }

  async addProduct(item: IProduct): Promise<void> {
    try {
      await save('insert into products (name, price) values (?, ?)',
        [`${item.name}`, `${item.price}`]);
    } catch (e) {
      console.error('error adding product');
      throw e;
    }
  }

  async removeProduct(id: number): Promise<void> {
    try {
      await remove('delete from products where id = ?', id);
    } catch (e) {
      console.error('error removing product');
      throw e;
    }
  }

  async isProductInShoppingCart(productId: number): Promise<boolean> {
    try {
      const results = await findOne('select product_id from shopping_cart where product_id = ?', `${productId}`);
      console.log(results);
      if (results && results.length > 0) {
        return true;
      }

      return false;
    } catch (e) {
      console.error('error finding all items in cart');
      throw e;
    }
  }
}