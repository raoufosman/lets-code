import { ICart, ICartItem } from '../models/cart.model';
import { findAll, save, remove } from './sqlite';

export class ShoppingCartRepository {
  async getCart(): Promise<ICart> {
    try {
      const result = await findAll(`
        select 
          s.id as shoppingCartItemId, 
          p.id as productId, 
          p.name, 
          p.price, 
          s.quantity 
        from shopping_cart s
        join products p on p.id = s.product_id`);

      if (result && result.length > 0) {
        return { items: result } as ICart;
      }

      return { items: [] } as ICart;
    } catch (e) {
      console.error('error finding all items in cart');
      throw e;
    }
  }

  async addItemToCart(item: ICartItem): Promise<void> {
    try {
      await save('insert into shopping_cart (product_id, quantity) values (?, ?)',
        [`${item.productId}`, `${item.quantity}`]);
    } catch (e) {
      console.error('error adding item to cart');
      throw e;
    }
  }

  async removeItemFromCart(id: number): Promise<void> {
    try {
      await remove('delete from shopping_cart where id = ?', id);
    } catch (e) {
      console.error('error adding item to cart');
      throw e;
    }
  }
}