import { ICart } from '../models/cart.model';

export const calculateCart = (cart: ICart) => {
  let totalPrice = 0;
  let totalQuantity = 0;

  if (cart.items && cart.items.length > 0) {
    cart.items.forEach(item => {
      totalQuantity += item.quantity;
      item.total = item.quantity * (item.price ?? 0);
      totalPrice += item.total;
    });

    if (totalQuantity > 2) {
      cart.discountApplied = true;
      var prices = cart.items.map(i => i.price ? i.price : 0);

      if (prices && prices.length) {
        if (totalQuantity <= 4) {
          var lowestPrice = Math.min(...prices);

          var itemInCart = cart.items.find(i => i.price === lowestPrice);

          if (itemInCart && itemInCart.price) {
            totalQuantity = totalQuantity - 1;
            totalPrice = Number((totalPrice - itemInCart.price).toFixed(2));
          }
        } else if (totalQuantity >= 5) {
          var lowest2prices = prices.sort((a, b) => a - b).slice(0, 2);
          var itemsInCart = cart.items.filter(i => lowest2prices.includes(i.price!));

          if (itemsInCart.length > 0) {
            totalQuantity = totalQuantity - 2;
            var totalDiscount = itemsInCart.map(i => i.price).reduce((p, n) => p! + n!);

            if (totalDiscount && totalDiscount > 0)
              totalPrice = Number((totalPrice - totalDiscount).toFixed(2));
          }
        }
      }
    }
  }

  return { totalPrice, totalQuantity };
};